﻿namespace VoteOnIt_WS.Common
{
    public static class Enums
    {
        public enum SurveyTypes
        {
            Checkboxes,
            Textboxes,
            Mixed
        }

        public enum UserRoles
        {
            Admin,
            RegisteredUser,
            Banned
        }
    }
}