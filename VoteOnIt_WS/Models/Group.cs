﻿using System;
using MongoDB.Bson.Serialization.Attributes;

namespace VoteOnIt_WS.Models
{
    public class Group
    {
        [BsonId]
        public string Id { get; set; }

        public string GroupName { get; set; }
        public bool? IsPrivate { get; set; }
        public string Code { get; set; }
        public string CreatedBy { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
    }
}