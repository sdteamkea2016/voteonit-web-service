﻿using System.Collections.Generic;

namespace VoteOnIt_WS.Models
{
    public class Question
    {
        public string Title { get; set; }

        public string Description { get; set; }
        public string Type { get; set; }
        public List<Option> Options { get; set; }
    }
}