﻿using System.Collections.Generic;

namespace VoteOnIt_WS.Models
{
    public class Answer
    {
        public int QuestionNumber { get; set; }
        public List<Choice> Choices { get; set; }
    }
}