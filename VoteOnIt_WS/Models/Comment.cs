﻿using System;

namespace VoteOnIt_WS.Models
{
    public class Comment
    {
        public string UserId { get; set; }
        public string Message { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public int Votes { get; set; }
    }
}