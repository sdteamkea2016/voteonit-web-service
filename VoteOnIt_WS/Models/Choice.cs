﻿namespace VoteOnIt_WS.Models
{
    public class Choice
    {
        public int OptionNumber { get; set; }
        public string Value { get; set; }
    }
}