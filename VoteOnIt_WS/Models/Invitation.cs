﻿using MongoDB.Bson;

namespace VoteOnIt_WS.Models
{
    public class Invitation
    {
        public ObjectId UserId { get; set; }
        public ObjectId GroupId { get; set; }
    }
}