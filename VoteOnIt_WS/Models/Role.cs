﻿using System;

namespace VoteOnIt_WS.Models
{
    public class Role
    {
        public string RoleName { get; set; }
        public string GrantedBy { get; set; }
        public DateTime DateGranted { get; set; }
    }
}