﻿using System;
using System.Collections.Generic;

namespace VoteOnIt_WS.Models
{
    public class Response
    {
        public string UserId { get; set; }
        public DateTime DateResponded { get; set; }
        public string Feedback { get; set; }
        public List<Answer> Answers { get; set; }
    }
}