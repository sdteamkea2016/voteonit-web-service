﻿namespace VoteOnIt_WS.Models
{
    public class Option
    {
        public string Name { get; set; }
        public bool IsCorrect { get; set; }
        public string Explanation { get; set; }
    }
}