﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Driver;
using VoteOnIt_WS.Interfaces;
using VoteOnIt_WS.Models;

namespace VoteOnIt_WS.Repositories
{
    public class SurveyRepository : ISurveyRepository
    {
        private readonly IMongoClient _client;
        private readonly IMongoDatabase _db;
        private readonly IMongoCollection<BsonDocument> _surveyCollection;

        protected internal SurveyRepository()
        {
            var con = ConfigurationManager.ConnectionStrings["MongoDBConnectionString"].ConnectionString;
            _client = new MongoClient(con);
            _db = _client.GetDatabase("voteonitdb");
            _surveyCollection = _db.GetCollection<BsonDocument>("Survey");
        }

        public bool CreateNewSurvey(Survey pNewSurvey)
        {
            var success = true;

            try
            {
                pNewSurvey.Id = ObjectId.GenerateNewId().ToString();
                pNewSurvey.DateCreated = DateTime.Now;

                _surveyCollection.InsertOneAsync(pNewSurvey.ToBsonDocument());
            }
            catch (Exception)
            {
                success = false;
            }

            return success;
        }

        public bool DeleteSurvey(string pId)
        {
            var success = true;
            var filter = Builders<BsonDocument>.Filter.Eq("_id", pId);

            try
            {
                _surveyCollection.DeleteOne(filter);
            }
            catch (Exception)
            {
                success = false;
            }

            return success;
        }

        public IEnumerable<Survey> GetAllSurveys()
        {
            var bsonSurveys = _surveyCollection.Find(new BsonDocument()).ToList();
            var surveys = new List<Survey>();

            foreach (var bsonSurvey in bsonSurveys)
            {
                var survey = new Survey();
                survey = BsonToSurvey(bsonSurvey);
                surveys.Add(survey);
            }

            return surveys;
        }

        public IEnumerable<Survey> GetAllSurveysByUser(string mail)
        {
            return _db.GetCollection<Survey>("Survey").Find(x => x.CreatedById == mail).ToList();
        }


        public Survey GetSurveyById(string pId)
        {
            Survey survey;

            var filter = Builders<BsonDocument>.Filter.Eq("_id", pId);
            var doc = _surveyCollection.Find(filter).FirstOrDefault();
            survey = doc == null ? null : BsonToSurvey(doc);

            return survey;
        }

        public bool UpdateSurvey(string pId, Survey pUpdatedSurvey)
        {
            var success = true;
            var filter = Builders<BsonDocument>.Filter.Eq("_id", pId);
            var update = Builders<BsonDocument>.Update.Set("DateModified", DateTime.Now);

            if (!string.IsNullOrEmpty(pUpdatedSurvey.CreatedById))
                update = update.Set("CreatedById", pUpdatedSurvey.CreatedById);
            if (!string.IsNullOrEmpty(pUpdatedSurvey.Title))
                update = update.Set("Title", pUpdatedSurvey.Title);
            if (!string.IsNullOrEmpty(pUpdatedSurvey.Description))
                update = update.Set("Description", pUpdatedSurvey.Description);
            if (pUpdatedSurvey.DateCreated != DateTime.MinValue)
                update = update.Set("DateCreated", pUpdatedSurvey.DateCreated);
            if (pUpdatedSurvey.ValidFrom != DateTime.MinValue)
                update = update.Set("ValidFrom", pUpdatedSurvey.ValidFrom);
            if (pUpdatedSurvey.ValidTo != DateTime.MinValue)
                update = update.Set("ValidTo", pUpdatedSurvey.ValidTo);
            update = update.Set("IsPrivate", pUpdatedSurvey.IsPrivate);

            if (pUpdatedSurvey.Tags != null)
            {
                update = update.Set("Tags", pUpdatedSurvey.Tags);
            }
            if (pUpdatedSurvey.Comments != null)
            {
                update = update.Set("Comments", pUpdatedSurvey.Comments);
            }
            if (pUpdatedSurvey.Invitations != null)
            {
                update = update.Set("Invitations", pUpdatedSurvey.Invitations);
            }
            if (pUpdatedSurvey.Questions != null)
            {
                update = update.Set("Questions", pUpdatedSurvey.Questions);
            }
            if (pUpdatedSurvey.Responses != null)
            {
                update = update.Set("Responses", pUpdatedSurvey.Responses);
            }

            try
            {
                _surveyCollection.UpdateOne(filter, update);
            }
            catch (Exception)
            {
                success = false;
            }

            return success;
        }

        private Survey BsonToSurvey(BsonDocument document)
        {
            var survey = new Survey();
            DateTime outDate;
            int outInt;
            bool outBool;

            survey.Id = document.Any(b => b.Name == "_id")
                ? document["_id"].ToString()
                : null;
            survey.CreatedById = document.Any(b => b.Name == "CreatedById")
                ? document["CreatedById"].ToString()
                : null;
            survey.Title = document.Any(b => b.Name == "Title")
                ? document["Title"].ToString()
                : null;
            survey.Description = document.Any(b => b.Name == "Description")
                ? document["Description"].ToString()
                : null;
            survey.IsPrivate = document.Any(b => b.Name == "IsPrivate")
                ? bool.TryParse(document["IsPrivate"].ToString(), out outBool) ? outBool : false
                : false;
            survey.DateCreated = document.Any(b => b.Name == "DateCreated")
                ? DateTime.TryParse(document["DateCreated"].ToString(), out outDate) ? outDate : DateTime.MinValue
                : DateTime.MinValue;
            survey.DateModified = document.Any(b => b.Name == "DateModified")
                ? DateTime.TryParse(document["DateModified"].ToString(), out outDate) ? outDate : DateTime.MinValue
                : DateTime.MinValue;
            survey.ValidFrom = document.Any(b => b.Name == "ValidFrom")
                ? DateTime.TryParse(document["ValidFrom"].ToString(), out outDate) ? outDate : DateTime.MinValue
                : DateTime.MinValue;
            survey.ValidTo = document.Any(b => b.Name == "ValidTo")
                ? DateTime.TryParse(document["ValidTo"].ToString(), out outDate) ? outDate : DateTime.MinValue
                : DateTime.MinValue;

            // reading tags
            var tagsList = new List<string>();
            if (document.Any(b => b.Name == "Tags") && document["Tags"].IsBsonArray)
            {
                foreach (var bsonTag in document["Tags"].AsBsonArray)
                {
                    string tag;
                    tag = bsonTag.AsBsonDocument.Where(b => b.Name == "Tag").Any()
                        ? bsonTag["Tag"].ToString()
                        : null;
                    if (!string.IsNullOrEmpty(tag))
                    {
                        tagsList.Add(bsonTag.ToString());
                    }
                }
            }
            survey.Tags = tagsList;

            // reading comments
            var commentsList = new List<Comment>();
            if (document.Any(b => b.Name == "Comments") && document["Comments"].IsBsonArray)
            {
                foreach (var bsonComment in document["Comments"].AsBsonArray)
                {
                    var comment = new Comment();
                    comment.UserId = bsonComment.AsBsonDocument.Any(b => b.Name == "UserId")
                        ? bsonComment["UserId"].ToString()
                        : null;
                    comment.Message = bsonComment.AsBsonDocument.Any(b => b.Name == "Message")
                        ? bsonComment["Message"].ToString()
                        : null;
                    comment.DateCreated = bsonComment.AsBsonDocument.Any(b => b.Name == "DateCreated")
                        ? DateTime.TryParse(bsonComment["DateCreated"].ToString(), out outDate)
                            ? outDate
                            : DateTime.MinValue
                        : DateTime.MinValue;
                    comment.DateModified = bsonComment.AsBsonDocument.Any(b => b.Name == "DateModified")
                        ? DateTime.TryParse(bsonComment["DateModified"].ToString(), out outDate)
                            ? outDate
                            : DateTime.MinValue
                        : DateTime.MinValue;
                    comment.Votes = bsonComment.AsBsonDocument.Any(b => b.Name == "Votes")
                        ? int.TryParse(bsonComment["Votes"].ToString(), out outInt) ? outInt : 0
                        : 0;

                    commentsList.Add(comment);
                }
            }
            survey.Comments = commentsList;

            // reading questions
            var questionsList = new List<Question>();
            if (document.Any(b => b.Name == "Questions") && document["Questions"].IsBsonArray)
            {
                foreach (var bsonQuestion in document["Questions"].AsBsonArray)
                {
                    var question = new Question();
                    question.Title = bsonQuestion.AsBsonDocument.Any(b => b.Name == "Title")
                        ? bsonQuestion["Title"].ToString()
                        : null;
                    question.Description = bsonQuestion.AsBsonDocument.Any(b => b.Name == "Description")
                        ? bsonQuestion["Description"].ToString()
                        : null;
                    question.Type = bsonQuestion.AsBsonDocument.Any(b => b.Name == "Type")
                        ? bsonQuestion["Type"].ToString()
                        : null;

                    // reading options
                    if (bsonQuestion.AsBsonDocument.Any(b => b.Name == "Options") && bsonQuestion["Options"].IsBsonArray)
                    {
                        var optionsList = new List<Option>();
                        foreach (var bsonOption in bsonQuestion["Options"].AsBsonArray)
                        {
                            var option = new Option();
                            option.Name = bsonOption.AsBsonDocument.Any(b => b.Name == "Name")
                                ? bsonOption["Name"].ToString()
                                : null;
                            option.IsCorrect = bsonOption.AsBsonDocument.Any(b => b.Name == "IsCorrect")
                                ? bool.TryParse(bsonOption["IsCorrect"].ToString(), out outBool) ? outBool : false
                                : false;
                            option.Explanation = bsonOption.AsBsonDocument.Any(b => b.Name == "Explanation")
                                ? bsonOption["Explanation"].ToString()
                                : null;

                            optionsList.Add(option);
                        }
                        question.Options = optionsList;
                    }

                    questionsList.Add(question);
                }
            }
            survey.Questions = questionsList;

            // reading responses
            var responsesList = new List<Response>();
            if (document.Any(b => b.Name == "Responses") && document["Responses"].IsBsonArray)
            {
                foreach (var bsonResponse in document["Responses"].AsBsonArray)
                {
                    var response = new Response();
                    response.UserId = bsonResponse.AsBsonDocument.Any(b => b.Name == "UserId")
                        ? bsonResponse["UserId"].ToString()
                        : null;
                    response.DateResponded = bsonResponse.AsBsonDocument.Any(b => b.Name == "DateResponded")
                        ? DateTime.TryParse(bsonResponse["DateResponded"].ToString(), out outDate)
                            ? outDate
                            : DateTime.MinValue
                        : DateTime.MinValue;
                    response.Feedback = bsonResponse.AsBsonDocument.Any(b => b.Name == "Feedback")
                        ? bsonResponse["Feedback"].ToString()
                        : null;

                    // reading answers
                    if (bsonResponse.AsBsonDocument.Any(b => b.Name == "Answers") && bsonResponse["Answers"].IsBsonArray)
                    {
                        var answersList = new List<Answer>();
                        foreach (var bsonAnswer in bsonResponse["Answers"].AsBsonArray)
                        {
                            var answer = new Answer();
                            answer.QuestionNumber = bsonAnswer.AsBsonDocument.Any(b => b.Name == "QuestionNumber")
                                ? int.TryParse(bsonAnswer["QuestionNumber"].ToString(), out outInt) ? outInt : -1
                                : -1;

                            // reading choices
                            if (bsonAnswer.AsBsonDocument.Any(b => b.Name == "Choices") &&
                                bsonAnswer["Choices"].IsBsonArray)
                            {
                                var choicesList = new List<Choice>();
                                foreach (var bsonChoice in bsonAnswer["Choices"].AsBsonArray)
                                {
                                    var choice = new Choice();
                                    choice.OptionNumber = bsonChoice.AsBsonDocument.Any(b => b.Name == "OptionNumber")
                                        ? int.TryParse(bsonChoice["OptionNumber"].ToString(), out outInt) ? outInt : -1
                                        : -1;
                                    choice.Value = bsonChoice.AsBsonDocument.Any(b => b.Name == "Value")
                                        ? bsonChoice["Value"].ToString()
                                        : null;
                                    choicesList.Add(choice);
                                }
                                answer.Choices = choicesList;
                            }
                            answersList.Add(answer);
                        }

                        response.Answers = answersList;
                    }

                    responsesList.Add(response);
                }
            }

            survey.Responses = responsesList;

            //public List<Invitation> Invitations { get; set; }

            return survey;
        }
    }
}