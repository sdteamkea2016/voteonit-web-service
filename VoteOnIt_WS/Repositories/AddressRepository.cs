﻿using VoteOnIt_WS.Interfaces;
using VoteOnIt_WS.Models;

namespace VoteOnIt_WS.Repositories
{
    public class AddressRepository : IAddressRepository
    {
        private readonly UserRepository _user = new UserRepository();

        public bool AddAddress(string pUserId, Address pNewAddress)
        {
            var success = true;

            pNewAddress.Country = pNewAddress.Country.Trim();
            pNewAddress.City = pNewAddress.City.Trim();
            pNewAddress.Street = pNewAddress.Street.Trim();
            pNewAddress.ZipCode = pNewAddress.ZipCode.Trim();

            var user = new User();
            user = _user.GetUserById(pUserId);
            user.Addresses.Add(pNewAddress);

            success = _user.UpdateUser(pUserId, user);

            return success;
        }

        public bool RemoveAddress(string pUserId, Address pRemovedAddress)
        {
            var success = true;

            pRemovedAddress.Country = pRemovedAddress.Country.Trim();
            pRemovedAddress.City = pRemovedAddress.City.Trim();
            pRemovedAddress.Street = pRemovedAddress.Street.Trim();
            pRemovedAddress.ZipCode = pRemovedAddress.ZipCode.Trim();

            var user = new User();
            user = _user.GetUserById(pUserId);
            var addressIndex = user.Addresses.IndexOf(pRemovedAddress);
            user.Addresses.RemoveAt(addressIndex);

            success = _user.UpdateUser(pUserId, user);

            return success;
        }
    }
}