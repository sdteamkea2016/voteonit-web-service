﻿using System;
using System.Linq;
using VoteOnIt_WS.Common;
using VoteOnIt_WS.Interfaces;
using VoteOnIt_WS.Models;

namespace VoteOnIt_WS.Repositories
{
    public class RoleRepository : IRoleRepository
    {
        public bool AssignRole(string pId, string pRole, string pGrantedBy, out string pMessage)
        {
            bool success;
            var errorMessage = string.Empty;
            var ur = new UserRepository();

            success = IsValidRole(pRole);

            if (success)
            {
                var newRole = new Role();
                newRole.RoleName = pRole;
                newRole.DateGranted = DateTime.Now;
                newRole.GrantedBy = pGrantedBy;

                var user = ur.GetUserById(pId);
                if (user != null)
                {
                    // check if user doesn't have this role
                    if (user.Roles.All(r => r.RoleName != pRole))
                    {
                        user.Roles.Add(newRole);
                        success = ur.UpdateUser(pId, user);
                    }
                    else
                    {
                        success = false;
                        errorMessage = $"User with id: {pId} already has this role: {pRole}.";
                    }
                }
                else
                {
                    success = false;
                    errorMessage = $"There is no user in the database with id: {pId}.";
                }
            }
            else
            {
                errorMessage = $"Provided role is not valid. Role: {pRole}.";
            }

            pMessage = errorMessage;
            return success;
        }

        public bool RemoveRole(string pId, string pRole, out string pMessage)
        {
            var errorMessage = string.Empty;
            var ur = new UserRepository();

            var success = IsValidRole(pRole);

            if (success)
            {
                var user = ur.GetUserById(pId);
                if (user != null)
                {
                    var roleIndex = user.Roles.IndexOf(user.Roles.FirstOrDefault(r => r.RoleName == pRole));
                    if (roleIndex != -1)
                    {
                        user.Roles.RemoveAt(roleIndex);
                        success = ur.UpdateUser(pId, user);
                    }
                    else
                    {
                        success = false;
                        errorMessage = $"User with id: {pId} doesn't have this role: {pRole}.";
                    }
                }
                else
                {
                    success = false;
                    errorMessage = $"There is no user in the database with id: {pId}.";
                }
            }
            else
            {
                errorMessage = $"Provided role is not valid. Role: {pRole}.";
            }

            pMessage = errorMessage;
            return success;
        }

        public Role CreateRoleByName(string pRoleName)
        {
            var role = new Role
            {
                RoleName = pRoleName,
                GrantedBy = "System",
                DateGranted = DateTime.Now
            };

            return role;
        }

        private bool IsValidRole(string pRole)
        {
            // checking if a new role is valid
            return ((Enums.UserRoles[]) Enum.GetValues(typeof(Enums.UserRoles))).Where(x => x.ToString() == pRole).Any();
        }
    }
}