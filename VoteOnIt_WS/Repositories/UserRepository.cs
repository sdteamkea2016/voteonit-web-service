﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Driver;
using VoteOnIt_WS.Common;
using VoteOnIt_WS.Interfaces;
using VoteOnIt_WS.Models;

namespace VoteOnIt_WS.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly IMongoClient _client;
        private readonly IMongoDatabase _db;
        private readonly IMongoCollection<BsonDocument> _userCollection;

        protected internal UserRepository()
        {
            var con = ConfigurationManager.ConnectionStrings["MongoDBConnectionString"].ConnectionString;
            _client = new MongoClient(con);
            _db = _client.GetDatabase("voteonitdb");
            _userCollection = _db.GetCollection<BsonDocument>("User");
        }

        // TODO: only temporary, can be done more effecient 
        public User LoginProcess(string email, string password)
        {
            var bsonUsers = _userCollection.Find(new BsonDocument()).ToList();
            return bsonUsers.Select(bsonUser => BsonToUser(bsonUser)).
                FirstOrDefault(user => user.Email == email && user.Password == password);
        }

        public bool AddUser(User pUser)
        {
            var success = true;

            var rr = new RoleRepository();
            var newRole = rr.CreateRoleByName(Enums.UserRoles.RegisteredUser.ToString());
            pUser.Roles = new List<Role> {newRole};

            try
            {
                pUser.Id = ObjectId.GenerateNewId().ToString();
                pUser.DateCreated = DateTime.Now;

                _userCollection.InsertOneAsync(pUser.ToBsonDocument());
            }
            catch (Exception)
            {
                success = false;
            }

            return success;
        }

        public IEnumerable<User> GetAllUsers()
        {
            var bsonUsers = _userCollection.Find(new BsonDocument()).ToList();
            var users = new List<User>();

            foreach (var bsonUser in bsonUsers)
            {
                var user = new User();
                user = BsonToUser(bsonUser);
                users.Add(user);
            }

            return users;
        }

        public User GetUserById(string pId)
        {
            User user;
            var filter = Builders<BsonDocument>.Filter.Eq("_id", pId);

            var bsonUser = _userCollection.Find(filter).FirstOrDefault();
            if (bsonUser != null)
            {
                user = BsonToUser(bsonUser);
            }
            else
            {
                user = null;
            }

            return user;
        }

        public IEnumerable<User> UsersInGroup(string pGroupId)
        {
            var users = new List<User>();
            var filter = Builders<BsonDocument>.Filter.ElemMatch("Groups",
                Builders<BsonDocument>.Filter.Eq("$id", pGroupId));
            var bsonUsers = _userCollection.Find(filter).ToList();

            foreach (var bsonUser in bsonUsers)
            {
                var user = new User();
                user = BsonToUser(bsonUser);
                users.Add(user);
            }

            return users;
        }

        public bool DeleteUser(string pId)
        {
            var success = true;
            var filter = Builders<BsonDocument>.Filter.Eq("_id", pId);

            try
            {
                _userCollection.DeleteOne(filter);
            }
            catch (Exception)
            {
                success = false;
            }

            return success;
        }

        public bool UpdateUser(string pId, User pUser)
        {
            var success = true;
            var filter = Builders<BsonDocument>.Filter.Eq("_id", pId);
            var update = Builders<BsonDocument>.Update.Set("DateModified", DateTime.Now);

            if (!string.IsNullOrEmpty(pUser.Password))
                update = update.Set("Password", pUser.Password);
            if (!string.IsNullOrEmpty(pUser.FirstName))
                update = update.Set("FirstName", pUser.FirstName);
            if (!string.IsNullOrEmpty(pUser.LastName))
                update = update.Set("LastName", pUser.LastName);
            if (!string.IsNullOrEmpty(pUser.Gender))
                update = update.Set("Gender", pUser.Gender);
            if (!string.IsNullOrEmpty(pUser.Email))
                update = update.Set("Email", pUser.Email);
            if (pUser.DateOfBirth != DateTime.MinValue)
                update = update.Set("DateOfBirth", pUser.DateOfBirth);
            if (pUser.Roles != null)
            {
                update = update.Set("Roles", pUser.Roles);
            }
            if (pUser.Addresses != null)
            {
                update = update.Set("Addresses", pUser.Addresses);
            }
            if (pUser.Groups != null)
            {
                update = update.Set("Groups", pUser.Groups);
            }

            try
            {
                _userCollection.UpdateOne(filter, update);
            }
            catch (Exception)
            {
                success = false;
            }

            return success;
        }

        public bool UserExists(string pUsername)
        {
            bool exists;
            var filter = Builders<BsonDocument>.Filter.Eq("Username", pUsername);

            exists = _db.GetCollection<BsonDocument>("User").Find(filter).Any();

            return exists;
        }

        public void AddFailedLoginAttempt(string pEmail)
        {
            var filter = Builders<BsonDocument>.Filter.Eq("Email", pEmail);
            var update = Builders<BsonDocument>.Update.Inc("LoginAttempts", 1);
            update = update.Set("DateModified", DateTime.Now);

            _userCollection.UpdateOne(filter, update);
        }

        public void ResetFailedLoginAttempts(string pUsername)
        {
            var filter = Builders<BsonDocument>.Filter.Eq("Username", pUsername);
            var update = Builders<BsonDocument>.Update.Set("LoginAttempts", 0);
            update = update.Set("DateModified", DateTime.Now);

            _userCollection.UpdateOne(filter, update);
        }

        public User GetUserByEmail(string pEmail)
        {
            var user = new User();
            var filter = Builders<BsonDocument>.Filter.Eq("Email", pEmail);

            user = BsonToUser(_userCollection.Find(filter).FirstOrDefault());

            return user;
        }

        public bool EmailExists(string email)
        {
            bool exists;
            var filter = Builders<BsonDocument>.Filter.Eq("Email", email);

            exists = _db.GetCollection<BsonDocument>("User").Find(filter).Any();

            return exists;
        }

        public bool UserExistsInGroup(string pUserId, string pGroupId)
        {
            var exists = false;

            var filter = Builders<BsonDocument>.Filter.Eq("Id", pUserId);
            filter = Builders<BsonDocument>.Filter.ElemMatch("Groups", Builders<BsonDocument>.Filter.Eq("$id", pGroupId));
            exists = _userCollection.Find(filter).Any();

            return exists;
        }

        private User BsonToUser(BsonDocument document)
        {
            var user = new User();

            DateTime outDate;
            int outInt;

            user.Id = document.Any(b => b.Name == "_id")
                ? document["_id"].ToString()
                : null;
            user.Username = document.Any(b => b.Name == "Username")
                ? document["Username"].ToString()
                : null;
            user.Password = document.Any(b => b.Name == "Password")
                ? document["Password"].ToString()
                : null;
            user.FirstName = document.Any(b => b.Name == "FirstName")
                ? document["FirstName"].ToString()
                : null;
            user.LastName = document.Any(b => b.Name == "LastName")
                ? document["LastName"].ToString()
                : null;
            user.Gender = document.Any(b => b.Name == "Gender")
                ? document["Gender"].ToString()
                : null;
            user.Email = document.Any(b => b.Name == "Email")
                ? document["Email"].ToString()
                : null;
            user.DateOfBirth = document.Any(b => b.Name == "DateOfBirth")
                ? DateTime.TryParse(document["DateOfBirth"].ToString(), out outDate) ? outDate : DateTime.MinValue
                : DateTime.MinValue;
            user.DateCreated = document.Any(b => b.Name == "DateCreated")
                ? DateTime.TryParse(document["DateCreated"].ToString(), out outDate) ? outDate : DateTime.MinValue
                : DateTime.MinValue;
            user.DateModified = document.Any(b => b.Name == "DateModified")
                ? DateTime.TryParse(document["DateModified"].ToString(), out outDate) ? outDate : DateTime.MinValue
                : DateTime.MinValue;
            user.LoginAttempts = document.Any(b => b.Name == "LoginAttempts")
                ? int.TryParse(document["LoginAttempts"].ToString(), out outInt) ? outInt : 0
                : 0;

            // reading user roles
            var rolesList = new List<Role>();
            if (document["Roles"].IsBsonArray)
            {
                foreach (var bsonRole in document["Roles"].AsBsonArray)
                {
                    var role = new Role();
                    role.RoleName = bsonRole.AsBsonDocument.Any(b => b.Name == "RoleName")
                        ? bsonRole["RoleName"].ToString()
                        : null;
                    role.GrantedBy = bsonRole.AsBsonDocument.Any(b => b.Name == "GrantedBy")
                        ? bsonRole["GrantedBy"].ToString()
                        : null;
                    role.DateGranted = bsonRole.AsBsonDocument.Any(b => b.Name == "DateGranted")
                        ? DateTime.TryParse(bsonRole["DateGranted"].ToString(), out outDate)
                            ? outDate
                            : DateTime.MinValue
                        : DateTime.MinValue;

                    rolesList.Add(role);
                }
            }
            user.Roles = rolesList;

            // reading user addresses
            var addressesList = new List<Address>();
            if (document["Addresses"].IsBsonArray)
            {
                foreach (var bsonAddress in document["Addresses"].AsBsonArray)
                {
                    var address = new Address();
                    address.Country = bsonAddress.AsBsonDocument.Any(b => b.Name == "Country")
                        ? bsonAddress["Country"].ToString()
                        : null;
                    address.City = bsonAddress.AsBsonDocument.Any(b => b.Name == "City")
                        ? bsonAddress["City"].ToString()
                        : null;
                    address.Street = bsonAddress.AsBsonDocument.Any(b => b.Name == "Street")
                        ? bsonAddress["Street"].ToString()
                        : null;
                    address.ZipCode = bsonAddress.AsBsonDocument.Any(b => b.Name == "ZipCode")
                        ? bsonAddress["ZipCode"].ToString()
                        : null;

                    addressesList.Add(address);
                }
            }
            user.Addresses = addressesList;

            // reading user groups
            var groupsList = new List<MongoDBRef>();
            if (document["Groups"].IsBsonArray)
            {
                foreach (var bsonGroup in document["Groups"].AsBsonArray)
                {
                    BsonValue id;
                    id = bsonGroup.AsBsonDocument.Any(b => b.Name == "$id")
                        ? bsonGroup["$id"].AsBsonValue
                        : null;

                    if (id != null)
                    {
                        var dbRef = new MongoDBRef("Group", id);
                        groupsList.Add(dbRef);
                    }
                }
            }
            user.Groups = groupsList;

            return user;
        }
    }
}