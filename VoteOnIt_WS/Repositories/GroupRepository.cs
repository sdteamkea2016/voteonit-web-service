﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Driver;
using VoteOnIt_WS.Interfaces;
using VoteOnIt_WS.Models;

namespace VoteOnIt_WS.Repositories
{
    public class GroupRepository : IGroupRepository
    {
        private readonly IMongoClient _client;
        private readonly IMongoDatabase _db;
        private readonly IMongoCollection<BsonDocument> _groupCollection;

        protected internal GroupRepository()
        {
            var con = ConfigurationManager.ConnectionStrings["MongoDBConnectionString"].ConnectionString;
            _client = new MongoClient(con);
            _db = _client.GetDatabase("voteonitdb");
            _groupCollection = _db.GetCollection<BsonDocument>("Group");
        }

        public bool AddUserToGroup(string pGroupId, string pUserId)
        {
            var success = false;
            var userRep = new UserRepository();

            var group = GetGroupById(pGroupId);
            var user = userRep.GetUserById(pUserId);
            if (user != null && group != null)
            {
                if (user.Groups == null)
                {
                    user.Groups = new List<MongoDBRef>();
                }
                var groupReference = new MongoDBRef("Group", pGroupId);
                if (!user.Groups.Contains(groupReference))
                {
                    user.Groups.Add(groupReference);
                    success = userRep.UpdateUser(pUserId, user);
                }
            }

            return success;
        }

        public bool CreateGroup(Group pGroup)
        {
            var success = true;

            try
            {
                pGroup.Id = ObjectId.GenerateNewId().ToString();
                pGroup.DateCreated = DateTime.Now;

                _groupCollection.InsertOneAsync(pGroup.ToBsonDocument());
            }
            catch (Exception)
            {
                success = false;
            }

            return success;
        }

        public bool DeleteGroup(string pGroupId)
        {
            var success = true;
            var filter = Builders<BsonDocument>.Filter.Eq("_id", pGroupId);

            try
            {
                _groupCollection.DeleteOne(filter);
            }
            catch (Exception)
            {
                success = false;
            }

            return success;
        }

        public IEnumerable<Group> GetAllGroups()
        {
            var bsonGroups = _groupCollection.Find(new BsonDocument()).ToList();
            var groups = new List<Group>();

            foreach (var bsonGroup in bsonGroups)
            {
                var group = new Group();
                group = BsonToGroup(bsonGroup);
                groups.Add(group);
            }

            return groups;
        }

        public IEnumerable<Group> GetAllGroups(string pUserId)
        {
            var groupsList = new List<Group>();
            var ur = new UserRepository();
            var user = ur.GetUserById(pUserId);
            foreach (var groupRef in user.Groups)
            {
                var group = GetGroupById(groupRef.Id.ToString());
                groupsList.Add(group);
            }

            return groupsList;
        }

        public Group GetGroupById(string pGroupId)
        {
            var group = new Group();
            var filter = Builders<BsonDocument>.Filter.Eq("_id", pGroupId);

            group = BsonToGroup(_groupCollection.Find(filter).FirstOrDefault());

            return group;
        }

        public bool GroupNameExists(string pGroupName)
        {
            bool exists;
            var filter = Builders<BsonDocument>.Filter.Eq("GroupName", pGroupName);

            exists = _db.GetCollection<BsonDocument>("Group").Find(filter).Any();

            return exists;
        }

        public bool RemoveUserFromGroup(string pGroupId, string pUserId)
        {
            var success = true;

            var ur = new UserRepository();
            var user = ur.GetUserById(pUserId);
            user.Groups.RemoveAll(x => x.Id.ToString() == pGroupId);
            success = ur.UpdateUser(pUserId, user);

            return success;
        }

        public bool UpdateGroup(string pGroupId, Group pUpdatedGroup)
        {
            var success = true;
            var filter = Builders<BsonDocument>.Filter.Eq("_id", pGroupId);
            var update = Builders<BsonDocument>.Update.Set("DateModified", DateTime.Now);

            if (!string.IsNullOrEmpty(pUpdatedGroup.GroupName))
                update = update.Set("GroupName", pUpdatedGroup.GroupName);
            if (!string.IsNullOrEmpty(pUpdatedGroup.Code))
                update = update.Set("Code", pUpdatedGroup.Code);
            if (pUpdatedGroup.IsPrivate != null)
                update = update.Set("IsPrivate", pUpdatedGroup.IsPrivate);

            try
            {
                _groupCollection.UpdateOne(filter, update);
            }
            catch (Exception)
            {
                success = false;
            }

            return success;
        }

        private Group BsonToGroup(BsonDocument document)
        {
            var group = new Group();
            DateTime outDate;

            group.Id = document.Where(b => b.Name == "_id").Any()
                ? document["_id"].ToString()
                : null;
            group.GroupName = document.Where(b => b.Name == "GroupName").Any()
                ? document["GroupName"].ToString()
                : null;
            group.IsPrivate = document.Where(b => b.Name == "IsPrivate").Any()
                ? (bool) document["IsPrivate"]
                : false;
            group.Code = document.Where(b => b.Name == "Code").Any()
                ? document["Code"].ToString()
                : null;
            group.CreatedBy = document.Where(b => b.Name == "CreatedBy").Any()
                ? document["CreatedBy"].ToString()
                : null;
            group.DateCreated = document.Where(b => b.Name == "DateCreated").Any()
                ? DateTime.TryParse(document["DateCreated"].ToString(), out outDate) ? outDate : DateTime.MinValue
                : DateTime.MinValue;
            group.DateModified = document.Where(b => b.Name == "DateModified").Any()
                ? DateTime.TryParse(document["DateModified"].ToString(), out outDate) ? outDate : DateTime.MinValue
                : DateTime.MinValue;

            return group;
        }
    }
}