﻿using System.Web.Http;
using VoteOnIt_WS.Interfaces;
using VoteOnIt_WS.Models;
using VoteOnIt_WS.Repositories;

namespace VoteOnIt_WS.Controllers
{
    public class AddressController : ApiController
    {
        private static readonly IAddressRepository _address = new AddressRepository();

        /// <summary>
        ///     Adds a new address to a specified user
        /// </summary>
        /// <param name="pUserID">ID of user</param>
        /// <param name="pNewAddress">Object of type Address</param>
        /// <returns>True - if operation was successful and False otherwise</returns>
        [HttpPut]
        [Route("api/address/add/{pNewAddress}")]
        public bool AddAddress(string pUserID, Address pNewAddress)
        {
            return _address.AddAddress(pUserID, pNewAddress);
        }

        /// <summary>
        ///     Removes an address from a specified user
        /// </summary>
        /// <param name="pUserID">ID of user</param>
        /// <param name="pRemovedUser">Object of type Address</param>
        /// <returns>True - if operation was successful and False otherwise</returns>
        [HttpPut]
        [Route("api/address/remove/{pUserId}/{pRemovedUser}")]
        public bool RemoveAddress(string pUserID, Address pRemovedUser)
        {
            return _address.RemoveAddress(pUserID, pRemovedUser);
        }
    }
}