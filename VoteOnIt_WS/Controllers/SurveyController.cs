﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using VoteOnIt_WS.Interfaces;
using VoteOnIt_WS.Models;
using VoteOnIt_WS.Repositories;

namespace VoteOnIt_WS.Controllers
{
    public class SurveyController : ApiController
    {
        private static readonly ISurveyRepository _surveys = new SurveyRepository();

        /// <summary>
        ///     Gets the entire list of all the surveys in a database
        /// </summary>
        /// <returns>List of type Survey</returns>
        [HttpGet]
        [Route("api/survey")]
        public IEnumerable<Survey> GetAllSurveys()
        {
            return _surveys.GetAllSurveys();
        }

        /// <summary>
        ///     Gets a specific survey by ID
        /// </summary>
        /// <param name="pId">ID of survey</param>
        /// <returns>Object of type Survey</returns>
        [HttpGet]
        [Route("api/survey/{pId}")]
        public Survey GetSurveyById(string pId)
        {
            return _surveys.GetSurveyById(pId);
        }

                   /// <summary>
          ///     Gets a list of surveys created by a specific User
          /// </summary>
          /// <returns>List of type Survey</returns>
         [HttpGet]
         [Route("api/survey/get/{mail}")]
         public IEnumerable<Survey> GetAllSurveysByUser(string mail)
          {
              return _surveys.GetAllSurveysByUser(mail);
          }
  


        /// <summary>
        ///     Creates a new survey
        /// </summary>
        /// <param name="pNewSurvey">Object of type Survey</param>
        /// <returns>True - if operation was successful and False - otherwise</returns>
        [HttpPost]
        [Route("api/survey")]
        public bool CreateNewSurvey(Survey pNewSurvey)
        {
            return _surveys.CreateNewSurvey(pNewSurvey);
        }

        /// <summary>
        ///     Update information about the existing survey
        /// </summary>
        /// <param name="pUpdatedSurvey">Object of type Survey</param>
        /// <returns>True - if operation was successful and False otherwise</returns>
        [HttpPut]
        [Route("api/survey")]
        public bool UpdateUser(Survey pUpdatedSurvey)
        {
            DateTime outDate;

            pUpdatedSurvey.CreatedById = !string.IsNullOrEmpty(pUpdatedSurvey.CreatedById)
                ? pUpdatedSurvey.CreatedById.Trim()
                : string.Empty;
            pUpdatedSurvey.Title = !string.IsNullOrEmpty(pUpdatedSurvey.Title)
                ? pUpdatedSurvey.Title.Trim()
                : string.Empty;
            pUpdatedSurvey.Description = !string.IsNullOrEmpty(pUpdatedSurvey.Description)
                ? pUpdatedSurvey.Description.Trim()
                : string.Empty;
            pUpdatedSurvey.DateCreated = DateTime.TryParse(pUpdatedSurvey.DateCreated.ToString(), out outDate)
                ? outDate
                : DateTime.MinValue;
            pUpdatedSurvey.ValidFrom = DateTime.TryParse(pUpdatedSurvey.ValidFrom.ToString(), out outDate)
                ? outDate
                : DateTime.MinValue;
            pUpdatedSurvey.ValidTo = DateTime.TryParse(pUpdatedSurvey.ValidTo.ToString(), out outDate)
                ? outDate
                : DateTime.MinValue;

            return _surveys.UpdateSurvey(pUpdatedSurvey.Id.Trim(), pUpdatedSurvey);
        }

        /// <summary>
        ///     Delete survey from database
        /// </summary>
        /// <param name="pId">Id of survey</param>
        /// <returns>True - if operation was successful and False otherwise</returns>
        [HttpDelete]
        [Route("api/survey/{pId}")]
        public bool DeleteSurvey(string pId)
        {
            return _surveys.DeleteSurvey(pId.Trim());
        }


        /// <summary>
        ///    Test
        /// </summary>
        /// <param name="pId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/test/{pId}")]
        public bool Test(string pId)
        {
            return true;
        }
    }
}