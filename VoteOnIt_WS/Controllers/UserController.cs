﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using VoteOnIt_WS.Interfaces;
using VoteOnIt_WS.Models;
using VoteOnIt_WS.Repositories;

namespace VoteOnIt_WS.Controllers
{
    public class UserController : ApiController
    {
        private static readonly IUserRepository _users = new UserRepository();

        /// <summary>
        ///     Gets the entire list of existing users
        /// </summary>
        /// <returns>List of users</returns>
        [HttpGet]
        [Route("api/user")]
        public IEnumerable<User> GetAllUsers()
        {
            return _users.GetAllUsers();
        }

        /// <summary>
        ///     Gets a specific user by ID
        /// </summary>
        /// <param name="pId">ID of user</param>
        /// <returns>Object of type User</returns>
        [HttpGet]
        [Route("api/user/{pId}")]
        public User GetUserById(string pId)
        {
            var user = _users.GetUserById(pId.Trim());

            return user;
        }

        /// <summary>
        ///     Gets a specific user by Email
        /// </summary>
        /// <param name="pEmail">Email of user</param>
        /// <returns>Object of type User</returns>
        [HttpGet]
        [Route("api/user/email/{pEmail}")]
        public User GetUserByEmail(string pEmail)
        {
            var user = _users.GetUserByEmail(pEmail);

            return user;
        }

        /// <summary>
        ///     Checks authentication of user by his email and password
        /// </summary>
        /// <param name="email">Email provided by user</param>
        /// <param name="password">Password provided by user</param>
        /// <returns>Object of type User</returns>
        [HttpGet]
        [Route("api/account/{email}/{password}")]
        public User CheckLogin(string email, string password)
        {
            // Check if user credentials match up
            var user = _users.LoginProcess(email, password);
            if (user != null)
            {
                if (user.LoginAttempts != 0)
                {
                    _users.ResetFailedLoginAttempts(user.Username);
                }
            }
            else
            {
                _users.AddFailedLoginAttempt(email);
            }

            return user;
        }

        /// <summary>
        ///     Checks if provided username exists in the database
        /// </summary>
        /// <param name="pUsername">Username provided by user</param>
        /// <returns>True - if username exists and False otherwise</returns>
        [HttpGet]
        [Route("api/user/exists/{pUserName}")]
        public bool UsernameExists(string pUsername)
        {
            return _users.UserExists(pUsername.Trim());
        }

        /// <summary>
        ///     Checks if provided email exists in the database
        /// </summary>
        /// <param name="email">Email provided by user</param>
        /// <returns>True - if email exists and False otherwise</returns>
        [HttpGet]
        [Route("api/user/email/exists/{email}")]
        public bool EmailExists(string email)
        {
            return _users.EmailExists(email.Trim());
        }

        /// <summary>
        ///     Gets a list of users who are members of specified group
        /// </summary>
        /// <param name="pGroupId">ID of group</param>
        /// <returns>List of users</returns>
        [HttpGet]
        [Route("api/user/group/{pGroupId}")]
        public IEnumerable<User> GetUsersInGroup(string pGroupId)
        {
            return _users.UsersInGroup(pGroupId);
        }

        /// <summary>
        ///     Check if user exists in the specified group
        /// </summary>
        /// <param name="pUserId">ID of user</param>
        /// <param name="pGroupId">ID of group</param>
        /// <returns>True - if user exists and False otherwise</returns>
        [HttpGet]
        [Route("api/user/group/exists/{pUserId}/{pGroupId}")]
        public bool UserExistsInGroup(string pUserId, string pGroupId)
        {
            return _users.UserExistsInGroup(pUserId, pGroupId);
        }

        /// <summary>
        ///     Inserts a new user to database
        /// </summary>
        /// <param name="pNewUser">Object of type User</param>
        /// <returns>True - if operation was successful and False otherwise</returns>
        [HttpPost]
        [Route("api/user/")]
        public bool AddUser(User pNewUser)
        {
            var newUser = new User();

            newUser.Username = pNewUser.Username.Trim();
            newUser.FirstName = pNewUser.FirstName.Trim() ?? null;
            newUser.LastName = pNewUser.LastName.Trim() ?? null;
            newUser.Password = pNewUser.Password.Trim();
            newUser.Email = pNewUser.Email.Trim();
            newUser.Gender = pNewUser.Gender.Trim() ?? null;
            newUser.DateOfBirth = pNewUser.DateOfBirth ?? null;

            return _users.AddUser(newUser);
        }

        /// <summary>
        ///     Updates information about the existing user
        /// </summary>
        /// <param name="pUpdatedUser">Object of type User</param>
        /// <returns>True - if operation was successful and False otherwise</returns>
        [HttpPut]
        [Route("api/user")]
        public bool UpdateUser(User pUpdatedUser)
        {
            DateTime outDateOfBirth;

            pUpdatedUser.Username = !string.IsNullOrEmpty(pUpdatedUser.Username)
                ? pUpdatedUser.Username.Trim()
                : string.Empty;
            pUpdatedUser.Password = !string.IsNullOrEmpty(pUpdatedUser.Password)
                ? pUpdatedUser.Password.Trim()
                : string.Empty;
            pUpdatedUser.FirstName = !string.IsNullOrEmpty(pUpdatedUser.FirstName)
                ? pUpdatedUser.FirstName.Trim()
                : string.Empty;
            pUpdatedUser.LastName = !string.IsNullOrEmpty(pUpdatedUser.LastName)
                ? pUpdatedUser.LastName.Trim()
                : string.Empty;
            pUpdatedUser.Gender = !string.IsNullOrEmpty(pUpdatedUser.Gender)
                ? pUpdatedUser.Gender.Trim()
                : string.Empty;
            pUpdatedUser.Email = !string.IsNullOrEmpty(pUpdatedUser.Email)
                ? pUpdatedUser.Email.Trim()
                : string.Empty;
            pUpdatedUser.DateOfBirth = pUpdatedUser.DateOfBirth != null
                ? DateTime.TryParse(pUpdatedUser.DateOfBirth.ToString(), out outDateOfBirth)
                    ? outDateOfBirth
                    : DateTime.MinValue
                : DateTime.MinValue;

            return _users.UpdateUser(pUpdatedUser.Id.Trim(), pUpdatedUser);
        }

        /// <summary>
        ///     Deletes user from database
        /// </summary>
        /// <param name="pId">Id of user</param>
        /// <returns>True - if operation was successful and False otherwise</returns>
        [HttpDelete]
        [Route("api/user/{pId}")]
        public bool DeleteUser(string pId)
        {
            return _users.DeleteUser(pId.Trim());
        }
    }
}