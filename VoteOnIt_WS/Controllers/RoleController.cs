﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using VoteOnIt_WS.Common;
using VoteOnIt_WS.Interfaces;
using VoteOnIt_WS.Repositories;

namespace VoteOnIt_WS.Controllers
{
    public class RoleController : ApiController
    {
        private static readonly IRoleRepository _roles = new RoleRepository();

        /// <summary>
        ///     Gets all available user roles
        /// </summary>
        /// <returns>List of strings</returns>
        [HttpGet]
        [Route("api/user/roles")]
        public List<string> GetUserRoles()
        {
            return (from object role in Enum.GetValues(typeof(Enums.UserRoles)) select role.ToString()).ToList();
        }

        /// <summary>
        ///     Assigns a role to a specific user
        /// </summary>
        /// <param name="pId">ID of user</param>
        /// <param name="pRole">Role name</param>
        /// <param name="pGrantedBy">ID of user who granted the role</param>
        /// <returns>A JSON array of results: success - True or False, error_message - message about error if one has occured</returns>
        [HttpPut]
        [Route("api/user/roles/{pId}/{pRole}/{pGrantedBy}")]
        public object AssignRole(string pId, string pRole, string pGrantedBy)
        {
            string message;

            var success = _roles.AssignRole(pId.Trim(), pRole.Trim(), pGrantedBy.Trim(), out message);

            var results = new[]
            {
                new {success, error_message = message}
            };

            return results;
        }

        /// <summary>
        ///     Removes a role from a secific user
        /// </summary>
        /// <param name="pId">ID of user</param>
        /// <param name="pRole">Role name</param>
        /// <returns>A JSON array of results: success - True or False, error_message - message about error if one has occured</returns>
        [HttpPut]
        [Route("api/user/roles/{pId}/{pRole}")]
        public object RemoveRole(string pId, string pRole)
        {
            string message;
            var success = _roles.RemoveRole(pId, pRole, out message);

            var results = new[]
            {
                new {success, error_message = message}
            };

            return results;
        }
    }
}