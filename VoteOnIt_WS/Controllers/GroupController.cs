﻿using System.Collections.Generic;
using System.Web.Http;
using VoteOnIt_WS.Interfaces;
using VoteOnIt_WS.Models;
using VoteOnIt_WS.Repositories;

namespace VoteOnIt_WS.Controllers
{
    public class GroupController : ApiController
    {
        private static readonly IGroupRepository _groups = new GroupRepository();

        /// <summary>
        ///     Gets the entire list of all the groups in a database
        /// </summary>
        /// <returns>List of type Group</returns>
        [HttpGet]
        [Route("api/group")]
        public IEnumerable<Group> GetAllGroups()
        {
            return _groups.GetAllGroups();
        }

        /// <summary>
        ///     Gets a list of groups for a specified user
        /// </summary>
        /// <param name="pUserId">ID of user</param>
        /// <returns>List of type Group</returns>
        [HttpGet]
        [Route("api/group/user/{pUserId}")]
        public IEnumerable<Group> GetAllGroups(string pUserId)
        {
            return _groups.GetAllGroups(pUserId);
        }

        /// <summary>
        ///     Gets a group by its ID
        /// </summary>
        /// <param name="pId">ID of group</param>
        /// <returns>Object of type Group</returns>
        [HttpGet]
        [Route("api/group/{pId}")]
        public Group GetGroupById(string pId)
        {
            var group = _groups.GetGroupById(pId.Trim());

            return group;
        }

        /// <summary>
        ///     Checks if group name already exists in the database
        /// </summary>
        /// <param name="pGroupName">Name of a group</param>
        /// <returns>True - if such group already exists and False otherwise</returns>
        [HttpGet]
        [Route("api/group/exists/{pGroupName}")]
        public bool GroupNameExists(string pGroupName)
        {
            return _groups.GroupNameExists(pGroupName);
        }

        /// <summary>
        ///     Creates a new group
        /// </summary>
        /// <param name="pNewGroup">Object of type Group</param>
        /// <returns>True - if operation was successful and False otherwise</returns>
        [HttpPost]
        [Route("api/group")]
        public bool CreateGroup(Group pNewGroup)
        {
            pNewGroup.GroupName = pNewGroup.GroupName.Trim();
            pNewGroup.IsPrivate = pNewGroup.IsPrivate;
            pNewGroup.Code = pNewGroup.Code.Trim();
            pNewGroup.CreatedBy = pNewGroup.CreatedBy.Trim();

            return _groups.CreateGroup(pNewGroup);
        }

        /// <summary>
        ///     Updates an existing group
        /// </summary>
        /// <param name="pUpdatedGroup">Object of type Group</param>
        /// <returns>True - if operation was successful and False otherwise</returns>
        [HttpPut]
        [Route("api/group/")]
        public bool UpdateGroup(Group pUpdatedGroup)
        {
            pUpdatedGroup.GroupName = !string.IsNullOrEmpty(pUpdatedGroup.GroupName.Trim())
                ? pUpdatedGroup.GroupName.Trim()
                : string.Empty;
            pUpdatedGroup.Code = !string.IsNullOrEmpty(pUpdatedGroup.Code.Trim())
                ? pUpdatedGroup.Code.Trim()
                : string.Empty;
            pUpdatedGroup.IsPrivate = pUpdatedGroup.IsPrivate;

            return _groups.UpdateGroup(pUpdatedGroup.Id.Trim(), pUpdatedGroup);
        }

        /// <summary>
        ///     Adds user to a specified group
        /// </summary>
        /// <param name="pGroupId">ID of group</param>
        /// <param name="pUserId">ID of user</param>
        /// <returns>True - if operation was successful and False otherwise</returns>
        [HttpPut]
        [Route("api/group/add/{pGroupId}/{pUserId}")]
        public bool AddUserToGroup(string pGroupId, string pUserId)
        {
            return _groups.AddUserToGroup(pGroupId, pUserId);
        }

        /// <summary>
        ///     Removes user from a specified group
        /// </summary>
        /// <param name="pGroupId">ID of group</param>
        /// <param name="pUserId">ID of user</param>
        /// <returns>True - if operation was successful and False otherwise</returns>
        [HttpPut]
        [Route("api/group/remove/{pGroupId}/{pUserId}")]
        public bool RemoveUserFromGroup(string pGroupId, string pUserId)
        {
            return _groups.RemoveUserFromGroup(pGroupId, pUserId);
        }

        /// <summary>
        ///     Deletes a group
        /// </summary>
        /// <param name="pId">ID of group</param>
        /// <returns>True - if operation was successful and False otherwise</returns>
        [HttpDelete]
        [Route("api/group/{pId}")]
        public bool DeleteGroup(string pId)
        {
            return _groups.DeleteGroup(pId.Trim());
        }
    }
}