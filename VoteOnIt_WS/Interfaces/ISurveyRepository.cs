﻿using System.Collections.Generic;
using VoteOnIt_WS.Models;

namespace VoteOnIt_WS.Interfaces
{
    internal interface ISurveyRepository
    {
        IEnumerable<Survey> GetAllSurveys();
        IEnumerable<Survey> GetAllSurveysByUser(string mail);
        Survey GetSurveyById(string pId);
        bool CreateNewSurvey(Survey pNewSurvey);
        bool DeleteSurvey(string pId);
        bool UpdateSurvey(string pId, Survey pSurvey);
    }
}