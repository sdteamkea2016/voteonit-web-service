﻿using VoteOnIt_WS.Models;

namespace VoteOnIt_WS.Interfaces
{
    internal interface IAddressRepository
    {
        bool AddAddress(string pUserId, Address pNewAddress);
        bool RemoveAddress(string pUserId, Address pRemovedAddress);
    }
}