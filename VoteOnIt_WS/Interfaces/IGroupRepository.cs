﻿using System.Collections.Generic;
using VoteOnIt_WS.Models;

namespace VoteOnIt_WS.Interfaces
{
    internal interface IGroupRepository
    {
        bool CreateGroup(Group pGroup);
        bool DeleteGroup(string pGroupId);
        bool AddUserToGroup(string pGroupId, string pUserId);
        bool RemoveUserFromGroup(string pGroupId, string pUserId);
        IEnumerable<Group> GetAllGroups();
        IEnumerable<Group> GetAllGroups(string pUserId);
        Group GetGroupById(string pGroupId);
        bool UpdateGroup(string pGroupId, Group pUpdatedGroup);
        bool GroupNameExists(string pGroupName);
    }
}