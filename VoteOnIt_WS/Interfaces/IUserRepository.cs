﻿using System.Collections.Generic;
using VoteOnIt_WS.Models;

namespace VoteOnIt_WS.Interfaces
{
    internal interface IUserRepository
    {
        User LoginProcess(string email, string password);
        IEnumerable<User> GetAllUsers();
        User GetUserById(string pId);
        User GetUserByEmail(string pEmail);
        bool AddUser(User pUser);
        bool DeleteUser(string pId);
        bool UpdateUser(string pId, User pUser);
        bool UserExists(string pUsername);
        bool EmailExists(string email);
        void AddFailedLoginAttempt(string pUsername);
        void ResetFailedLoginAttempts(string pUsername);
        IEnumerable<User> UsersInGroup(string pGroupId);
        bool UserExistsInGroup(string pUserId, string pGroupId);
    }
}