﻿using VoteOnIt_WS.Models;

namespace VoteOnIt_WS.Interfaces
{
    internal interface IRoleRepository
    {
        bool RemoveRole(string pId, string pRole, out string pMessage);
        bool AssignRole(string pId, string pRole, string pGrantedBy, out string pMessage);
        Role CreateRoleByName(string pRoleName);
    }
}